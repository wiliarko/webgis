<link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css" />
<!-- Make sure you put this AFTER Leaflet's CSS -->
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>
   <script src="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.js"></script>
 <script src="assets/js/leaflet-panel-layers-master/src/leaflet-panel-layers.js"></script>
 <script src="assets/js/leaflet.ajax.js"></script>
 <script src="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.js"></script>
 <link rel="stylesheet" href="assets/js/Leaflet.markercluster-master/dist/MarkerCluster.css" />
 <link rel="stylesheet" href="assets/js/Leaflet.markercluster-master/dist/MarkerCluster.Default.css" />
 <link rel="stylesheet" href="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.css" />

 <script src="assets/js/Leaflet.markercluster-master/dist/leaflet.markercluster-src.js"></script>

<script type="text/javascript">
    let latLng=[-7.433099, 112.712702];
	var rute = [];
	var rs_order = [];
	var jarak_sort = [];
	var garis = [];

   	var map = L.map('mapid').setView(latLng, 12);

   	var LayerKita= L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
    });
	map.addLayer(LayerKita);

	$('#namars').on('change', function() {
		rander_jarak_waktu();
	});
	
	
	function getLocation() {
	  if (navigator.geolocation) {
	    navigator.geolocation.getCurrentPosition(showPosition);
	  } else {
	    x.innerHTML = "Geolocation is not supported by this browser.";
	  }
	}

	function showPosition(position) {
	  $("[name=latNow]").val(position.coords.latitude);
	  $("[name=lngNow]").val(position.coords.longitude);
	   let latLng = [position.coords.latitude, position.coords.longitude];

		control.spliceWaypoints(0, 1, latLng);
		 map.panTo(latLng);
	}

	
	getLocation()

	var markers = L.markerClusterGroup();
	let distanceTo = [];
	let latitudeLongitude = []
	

<?php

if($tipe!='pilih tipe' ){
	$db->where('tipe','%'. $tipe,'LIKE');
	
}
if($jn_rs!='pilih jenis rumah sakit'){
	$db->where('jn_rs','%'. $jn_rs,'LIKE');
}
if ($nm_rs != 'pilih rumah sakit') {
	$db->where('nm_rs', '%' . $nm_rs, 'LIKE');
}



	$db->join('m_kecamatan b','a.id_kecamatan=b.id_kecamatan','LEFT');
			$getdata=$db->ObjectBuilder()->get('rs a');
			echo "var rs = ".json_encode($getdata);
			foreach ($getdata as $row) {	
			?>
			
			latitudeLongitude.push([<?= $row->lat ?>, <?= $row->lng ?>])
			markers.addLayer(L.marker([<?=$row->lat?>,<?=$row->lng?>])
			.bindPopup("Nama Rumah Sakit :<?=$row->nm_rs?><br>"+
                       "Alamat :<?=$row->alamat?>,"+
                       "Kec. <?=$row->nm_kecamatan?><br>"+
                       "Tipe :<?=$row->tipe?><br>"+
					   "Jenis Rumah Sakit :<?=$row->jn_rs?><br>"+
					   "Jumlah Pelayanan :<?=$row->jml_pelayanan?><br>"+
					   "Email:<?=$row->email?><br>"+
					   "No Telepon :<?=$row->no_tlp?><br>"+
                       "<button class='btn btn-info' onclick='return keSini(<?=$row->lat?>,<?=$row->lng?>)'>Ke Sini</button>").openPopup());
			
					   markers.addLayer(markers);
			<?php
			}
			?>
			
	map.addLayer(markers);

	var control = L.Routing.control({
	    waypoints: [
	        latLng],
			lineOptions: {
			styles: [{
				color: 'green',
				opacity: 2,
				weight: 6
			}]
		},
	    routeWhileDragging: true,
		addWaypoints:false,
		reverseWaypoints: true,
		showAlternatives: true,
		altLineOptions: {
			styles: [
				{color: 'black', opacity: 2, weight: 6},
				{color: 'white', opacity: 2, weight: 6},
				{color: 'blue', opacity: 2, weight: 6}
			]
		}
	}).on('routesfound',function (e){
		for(i in rs){
			if(e.waypoints[0].latLng.lat==rs[i].lat || e.waypoints[1].latLng.lat==rs[i].lat){
				rs[i]['summary']=e.routes[0].summary;
				rs[i]['jarak']=e.routes[0].summary.totalDistance/1000;
				rs[i]['waktu']=(e.routes[0].summary.totalTime/60).toFixed(0);
				rs[i]['coordinates']=e.routes[0].coordinates;
				jarak_sort.push(rs[i]['jarak']);
				rander_garis(e.routes[0].coordinates);
			}
		}
		// let jarak = e.routes[0].summary.totalDistance
		// let kiloMeter = jarak / 1000
		// let waktu = e.routes[0].summary.totalTime
		// let menit = waktu / 60
	})

	control.addTo(map);


function keSini(lat, lng) {
  var latLng = L.latLng(lat, lng);
  control.spliceWaypoints(control.getWaypoints().length - 1, 1, latLng);
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function orderRs(){
	rs_order=[];
	var jarak_rs = jarak_sort.sort(function(a, b){return a - b});
	for(k in jarak_rs){
		for(l in rs){
			if(rs[l].jarak==jarak_rs[k]){
				rs_order.push(rs[l]);
			}
		}
	}
}

function rander_garis(data){
	garis=[];
	for(i in data){
		if(i>0){
			var pointA = new L.LatLng(data[i-1].lat, data[i-1].lng);
			var pointB = new L.LatLng(data[i].lat, data[i].lng);
			var pointList = [pointA, pointB];

			var firstpolyline = new L.Polyline(pointList, {
				color: 'red',
				weight: 4,
				opacity: 1.5,
				smoothFactor: 1
			});
			firstpolyline.addTo(map);
			garis.push(firstpolyline);
			
			L.marker([data[i].lat, data[i].lng]).addTo(map);
		}
	}
}

function rander_jarak_waktu(){
	var data = rs_order[$("#namars").val()];
	document.querySelector('input[name="jarak"]').value = `${data.jarak.toFixed(1)} km`;
	document.querySelector('input[name="waktu"]').value = `${data.waktu} min`;

	let latLng = [$("[name=latNow]").val(), $("[name=lngNow]").val()];
	control.setWaypoints([
		latLng,
		L.latLng(data.lat, data.lng)
	]);
}

function rander_select(){
	$("#namars").html("");
	for(j in rs_order){
		$("#namars").append('<option value="'+j+'">'+rs_order[j].nm_rs+' ('+(rs_order[j].jarak).toFixed(1)+' KM)'+'</option>');	
	}
	showBtn();
	rander_jarak_waktu();
}

$(document).on("click", ".dariSini", function () {
	showLoading();
	let latLng = [$("[name=latNow]").val(), $("[name=lngNow]").val()];
	control.spliceWaypoints(0, 1, latLng);
	map.panTo(latLng);

	var nearestDestination;
	var nearestDistance = Infinity;
	var selectRs;
	var selectElement = document.getElementById('namars');

	var i = 0;
	setInterval(function () {
		if(i < latitudeLongitude.length){
			control.setWaypoints([
				latLng,
				L.latLng(latitudeLongitude[i][0], latitudeLongitude[i][1])
			]);
		}else if(i == latitudeLongitude.length){
			orderRs();
			rander_select();
		}
		i++;
	}, 2000);

  	// for (let i = 0; i < latitudeLongitude.length; i++) {
	// 	let point1 = L.latLng(latLng[0], latLng[1]);
	// 	let point2 = L.latLng(latitudeLongitude[i][0], latitudeLongitude[i][1])

	// 	let distance = point1.distanceTo(point2);
	// 	var distanceKm = distance / 1000;
	// 	var distanceFixed = distanceKm.toFixed(1)

	// 	if (distance < nearestDistance) {
	// 		nearestDistance = distance;
	// 		nearestDestination = [latitudeLongitude[i][0], latitudeLongitude[i][1]];
	// 		selectRs=i;
	// 	}
		
	// 	var select = document.getElementById('namars');
	// 	// 	// console.log(x.options[1]);
	// 	var option = select.options[i];
	// 	option.text = `${'     '+option.text+distanceFixed+ 'km'}`;
	// 	// keSini(latitudeLongitude[i][0],latitudeLongitude[i][1]);
	// }

	// console.log(nearestDestination);
	// keSini(nearestDestination[0],nearestDestination[1]);
})

function showBtn() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("dariSini").style.display = "block";
}

function showLoading() {
  document.getElementById("loader").style.display = "block";
  document.getElementById("dariSini").style.display = "none";
}
</script>
